package br.tim.ecm.uploadpdfcontent.otcsws;

public enum OTCSWSResourceType {

    JSON, BYTE_ARRAY, RESOURCE
}
