package br.tim.ecm.uploadpdfcontent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@SpringBootApplication
public class UploadPdfContentApplication implements CommandLineRunner {

    public static void main(String[] args) {

        SpringApplication app = new SpringApplication(UploadPdfContentApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);

    }

    @Autowired
    ProcessFile processFile;

    @Override
    public void run(String... args) throws Exception {
        Files.walk(Paths.get("/Users/silvio/pdf-test"))
                .forEach(f -> {
                    File file = new File(f.toString());
                    if (file.isFile())
                        processFile.processFile(file);
                });
    }
}
