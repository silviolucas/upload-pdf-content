package br.tim.ecm.uploadpdfcontent.conf;

import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.IdleConnectionEvictor;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.concurrent.TimeUnit;

@Configuration
public class HTTPConnectionConfig {

	@Value("${http.total.max:200}")
	private int maxConTotal;

	@Value("${http.route.max:50}")
	private int maxConPerRouteTotal;

	@Value("${http.inactivity.ms:10000}")
	private int inactivityCheck;

	@Value("${http.con.timeout:5000}")
	private int connectionTimeout;

	@Value("${http.requestConnection.timeout:10000}")
	private int requestTimeout;

	@Value("${http.socket.timeout:5000}")
	private int socketTimeout;

	@Value("${http.evt.sleep:60000}")
	private int evtSleep;

	@Value("${http.evt.maxidle:30000}")
	private int evtIdle;

	@Value("${http.evt.enabled:true}")
	private boolean evtEnabled;

	@Bean
	public PoolingHttpClientConnectionManager getConnectionManager()
	{	
		PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
		connManager.setDefaultMaxPerRoute(maxConPerRouteTotal);
		connManager.setValidateAfterInactivity(inactivityCheck);			
		connManager.setMaxTotal(maxConTotal);
		if(evtEnabled)
		{
			IdleConnectionEvictor iv = new IdleConnectionEvictor(connManager,  evtSleep, TimeUnit.MILLISECONDS, evtIdle, TimeUnit.MILLISECONDS);
			iv.start();
		}
		return connManager;			
	}


	@Bean
	@Scope(value="prototype")
	public CloseableHttpClient getConnection(PoolingHttpClientConnectionManager conManager)
	{	
		//avoid missing keep alive header problem in the response
		ConnectionKeepAliveStrategy myStrategy = new ConnectionKeepAliveStrategy() {
			@Override
			public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
				HeaderElementIterator it = new BasicHeaderElementIterator
						(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
				while (it.hasNext()) {
					HeaderElement he = it.nextElement();
					String param = he.getName();
					String value = he.getValue();
					if (value != null && param.equalsIgnoreCase
							("timeout")) {
						return Long.parseLong(value) * 1000;
					}
				}
				return 5 * 1000;
			}
		};

		CloseableHttpClient client = HttpClients.custom().
				setConnectionManager(conManager)
				.setConnectionManagerShared(true)
				.setKeepAliveStrategy(myStrategy)
				.setDefaultRequestConfig(	
						RequestConfig.custom()
						.setConnectionRequestTimeout(requestTimeout)
						.setConnectTimeout(connectionTimeout)
						.setSocketTimeout(socketTimeout)			    	    
						.build())
				.setConnectionManager(conManager).build();

		return client;
	}

}
