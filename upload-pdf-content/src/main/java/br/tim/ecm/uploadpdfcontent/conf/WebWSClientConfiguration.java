
package br.tim.ecm.uploadpdfcontent.conf;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.annotation.PostConstruct;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;
import javax.xml.ws.soap.MTOMFeature;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Configuration
public class WebWSClientConfiguration extends WebMvcConfigurerAdapter {

	@Value("${ws.url.cs.authentication:}")
	private String contentServerAuth;
	@Value("${ws.url.cs.documentManagement:}")
	private String contentServerDocumentManagement;
	@Value("${ws.url.cs.contentservice:}")
	private String contentServerContentService;

	@Value("${ws.url.cs.connecttimeout:5000}")
	private int contentServerConnectTimeout;
	@Value("${ws.url.cs.timeout:5000}")
	private int contentServerTimeout;


	@Value("${ws.url.consultInvoice:}")
	private String consultInvoice;	
	@Value("${ws.user.consultInvoice:}")
	private String consultInvoiceUser;	
	@Value("${ws.pass.consultInvoice:}")
	private String consultInvoicePass;
	@Value("${ws.connectTimeout.consultInvoice:2000}")
	private int consultInvoiceConnectTimeout;
	@Value("${ws.timeout.consultInvoice:2000}")
	private int consultInvoiceTimeout;

	//IMDB

	@Value("${ws.url.msisdnstatus:}")
	private String msisdnStatus;
	@Value("${ws.user.msisdnstatus:}")
	private String msisdnStatusUser;
	@Value("${ws.pass.msisdnstatus:}")
	private String msisdnStatusPass;
	@Value("${ws.connectTimeout.msisdnstatus:2000}")
	private int msisdnStatusConnectTimeout;
	@Value("${ws.timeout.msisdnstatus:2000}")
	private int msisdnStatusTimeout;

	@Value("${ws.url.createBarcode:}")
	private String createBarcode;
	@Value("${ws.user.createBarcode:}")
	private String createBarcodeUser;
	@Value("${ws.pass.createBarcode:}")
	private String createBarcodePass;
	@Value("${ws.connectTimeout.createBarcode:2000}")
	private int createBarcodeConnectTimeout;
	@Value("${ws.timeout.createBarcode:2000}")
	private int createBarcodeTimeout;

	@Value("${ws.url.customerInvoiceList:}")
	private String customerInvoiceList;
	@Value("${ws.user.customerInvoiceList:}")
	private String customerInvoiceListUser;
	@Value("${ws.pass.customerInvoiceList:}")
	private String customerInvoiceListPass;
	@Value("${ws.connectTimeout.customerInvoiceList:2000}")
	private int customerInvoiceListConnectTimeout;
	@Value("${ws.timeout.customerInvoiceList:2000}")
	private int customerInvoiceListTimeout;
	
	@Value("${ws.url.postpaidBill:}")
	private String postpaidBill;
	@Value("${ws.user.postpaidBill:}")
	private String postpaidBillUser;
	@Value("${ws.pass.postpaidBill:}")
	private String postpaidBillPass;
	@Value("${ws.connectTimeout.postpaidBill:2000}")
	private int postpaidBillConnectTimeout;
	@Value("${ws.timeout.postpaidBill:2000}")
	private int postpaidBillTimeout;

	@Value("${ws.url.billProfileFull:}")
	private String billprofileFull;
	@Value("${ws.user.billProfileFull:}")
	private String billprofileFullUser;
	@Value("${ws.pass.billProfileFull:}")
	private String billprofileFullPass;
	@Value("${ws.connectTimeout.billProfileFull:2000}")
	private int billProfileFullConnectTimeout;
	@Value("${ws.timeout.billProfileFull:2000}")
	private int billProfileFullTimeout;

	@Value("${ws.url.protocolo:}")
	private String protocolo;
	@Value("${ws.user.protocolo:}")
	private String protocoloUser;
	@Value("${ws.pass.protocolo:}")
	private String protocoloPass;
	@Value("${ws.connectTimeout.protocolo:2000}")
	private int protocoloConnectTimeout;
	@Value("${ws.timeout.protocolo:2000}")
	private int protocoloTimeout;

	@Value("${rest.connection.timeout:8000}")
	private int restConnectionTimeout;
	@Value("${rest.read.timeout:8000}")
	private int restTemplateReadTimeout;
	

	@Bean
	public CloseableHttpClient httpClient(PoolingHttpClientConnectionManager poolingHttpClientConnectionManager) {
		CloseableHttpClient result = HttpClientBuilder
				.create()
				.setConnectionManager(poolingHttpClientConnectionManager)
				.build();
		return result;
	}
	
	 @Bean(name="customRestTemplate")
	 public RestTemplate restTemplate(CloseableHttpClient getConnection) {
		 
		 HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		 httpRequestFactory.setHttpClient(getConnection);
		 httpRequestFactory.setConnectTimeout(restConnectionTimeout);
		 httpRequestFactory.setReadTimeout(restTemplateReadTimeout);

		 return new RestTemplate(httpRequestFactory);
		 
	 }

}