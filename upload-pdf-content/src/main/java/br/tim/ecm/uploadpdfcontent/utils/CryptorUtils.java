package br.tim.ecm.uploadpdfcontent.utils;

import com.google.common.base.Strings;
import com.google.common.io.BaseEncoding;
import com.sun.crypto.provider.SunJCE;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

@SuppressWarnings("restriction")
public class CryptorUtils {
	Cipher ecipher;
	Cipher dcipher;
	private final String algoritm = "Blowfish";
	static private Log log = LogFactory.getLog(CryptorUtils.class);

	/**
	 * 
	 * @param msg
	 * @return
	 */
	public CryptorUtils(String keyString)  {
	
		try {
			javax.crypto.SecretKey key = new SecretKeySpec(keyString.getBytes(), algoritm);
			ecipher = Cipher.getInstance(algoritm);
			dcipher = Cipher.getInstance(algoritm);
			ecipher.init(Cipher.ENCRYPT_MODE, key);
			dcipher.init(Cipher.DECRYPT_MODE, key);
		}
		catch (NoSuchPaddingException e) {
			log.error(e);
			throw new RuntimeException(e);
		}
		catch (NoSuchAlgorithmException e) {
			log.error(e);
			throw new RuntimeException(e);
		}
		catch (InvalidKeyException e) {
			log.error(e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * 
	 * @param bytes
	 * @return
	 */
	private String encryptBytes(byte bytes[])  {
		try {
			byte enc[] = ecipher.doFinal(bytes);
			return BaseEncoding.base64().encode(enc);
		}
		catch (BadPaddingException e) {
			log.error(e);
			throw new RuntimeException(e);
		}
		catch (IllegalBlockSizeException e) {
			log.error(e);
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 
	 * @param bytes
	 * @return
	 */

	private byte[] decryptBytes(String input)   {
		try {
			byte dec[] = BaseEncoding.base64().decode(input);
			dec = dcipher.doFinal(dec);
			return dec;
		}
		catch (BadPaddingException e) {
			throw new RuntimeException(e);
		}
		catch (IllegalBlockSizeException e) {
			throw new RuntimeException(e);
		}

	}


	/**
	 * 
	 * @param msg
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public String encryptStr (String msg) throws UnsupportedEncodingException {
		
		if(Strings.isNullOrEmpty(msg))
			return null;
		
		String encrypted = "";
		Security.addProvider(new SunJCE());

		encrypted = encryptBytes(msg.getBytes("UTF-8"));
		return encrypted;
	}
	/**
	 * 
	 * @param msg
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public String decryptStr (String msg) throws UnsupportedEncodingException {
		if(Strings.isNullOrEmpty(msg))
			return null;
			
		String decrypted = "";

		Security.addProvider(new SunJCE());
		decrypted = new String(decryptBytes(msg));
		
		return decrypted;
	}
	
	public String decryptOrReturnSame(String msg){
		try{			
			return decryptStr(msg);
		} catch (Exception e){
			return msg;
		}
	
	}

}
