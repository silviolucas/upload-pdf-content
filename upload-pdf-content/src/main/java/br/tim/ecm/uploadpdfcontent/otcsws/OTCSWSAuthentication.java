package br.tim.ecm.uploadpdfcontent.otcsws;

import br.tim.ecm.uploadpdfcontent.otcsws.vo.OTCSWSConnectionResponse;
import br.tim.ecm.uploadpdfcontent.utils.CryptorUtils;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

/**
 * OTCSWS Authentication REST API functions
 * @author daniel.pereira
 *
 */
@Service
public class OTCSWSAuthentication {

	@Autowired
	private OTCSWSConnection connection;
	
	@Autowired
	private Environment env;

	private static String PROP_USERNAME = "ws.url.cs.rest.username.%s";

	private static String PROP_PASSWORD = "ws.url.cs.rest.password.%s";
	
	private final static String AUTH_ENDPOINT = "/api/v1/auth";

	private final ConcurrentHashMap<String, AtomicReference<String>> tokenReference = new ConcurrentHashMap<>();
	private final ReentrantLock reentrantLock = new ReentrantLock();

	@Value("${lock.timeout:40000}")
	private long lockTimeout;


	private static Logger LOGGER = LoggerFactory.getLogger(OTCSWSAuthentication.class);
	
	/**
	 * Number of retries to be executed if authentication fail
	 */
	private static final int AUTH_RETRIES = 5;
	
	/**
	 * Gets a new authentication token
	 * @param username
	 * @param password
	 * @param context
	 * @param restTemplate
	 * @return
	 */
	private String authenticate(String contentServerId) throws IOException  {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

		MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<String, String>();
		String username = this.getProperty(PROP_USERNAME, contentServerId);
		CryptorUtils crypto = new CryptorUtils("T1mC0nT4");
		String password = this.getProperty(PROP_PASSWORD, contentServerId);
		password = crypto.decryptOrReturnSame(password);
		
		requestBody.add("username", username);
		requestBody.add("password", password);

		int i = 0;
		
		OTCSWSConnectionResponse response = this.connection.exchange(AUTH_ENDPOINT, HttpMethod.POST, requestHeaders, requestBody, null, OTCSWSResourceType.JSON, contentServerId, i++);

		Object payload = response.getPayload();

		OTCSWSAuthVO auth = new Gson().fromJson( (String) payload, OTCSWSAuthVO.class);

		if( Strings.isNullOrEmpty(auth.getTicket()) && i < AUTH_RETRIES ){
			response = this.connection.exchange(AUTH_ENDPOINT, HttpMethod.POST, requestHeaders, requestBody, null, OTCSWSResourceType.JSON, contentServerId, ++i);
		}
		
		LOGGER.info("[OTCSWSAuthentication] AuthPayload: " + payload.toString());
		
		LOGGER.info( String.format("Logging with user [%s] and password [%s]. Service id [%s].", username, password, this.toString()) );
		String ticket = auth.getTicket();
		tokenReference.put(contentServerId, new AtomicReference<String>(ticket));

		return ticket;

	}

	/**
	 * Gets or refreshs the token
	 * @return
	 * @throws InterruptedException
	 */
	public String getToken(String contentServerId) throws InterruptedException, TimeoutException, IOException {

		AtomicReference<String> token = tokenReference.get(contentServerId);
		if(token != null) {
			return token.get();
		}

		if (!reentrantLock.tryLock(lockTimeout, TimeUnit.MILLISECONDS)) {
			// attempt to get the token, because some thread must set the token while this thread was locked
			token = tokenReference.get(contentServerId);
			if(token != null) {
				return token.get();
			}
			throw new TimeoutException("Cannot acquire lock to authenticate on server:" + contentServerId);
		}
		try {
			// attempt to get the token, because some thread must set the token while this thread was locked
			token = tokenReference.get(contentServerId);
			if(token != null) {
				return token.get();
			}
			String refreshToken = this.authenticate(contentServerId);
			return refreshToken;
		} finally {
			reentrantLock.unlock();
		}
	}

	/**
	 * Expires token
	 */
	public void expireToken(String contentId){
		LOGGER.info( String.format("Expiring the token due to timeout") );
		tokenReference.remove(contentId);
	}

	/**
	 * Gets property by contentServerId
	 * @param properties
	 * @param contentServerId
	 * @return
	 */
	 private String getProperty(String properties, String contentServerId){
    	String property = String.format(properties, contentServerId.toLowerCase());
		String value = env.getProperty(property);
		if(value != null){
			return value;
		}
		throw new RuntimeException("property doesn't exist: " + property);
	 }
	 
}
