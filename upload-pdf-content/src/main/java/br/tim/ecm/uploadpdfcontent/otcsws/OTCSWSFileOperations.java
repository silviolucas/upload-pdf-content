package br.tim.ecm.uploadpdfcontent.otcsws;

import br.tim.ecm.uploadpdfcontent.otcsws.exception.OTCSWSFileExceptions;
import br.tim.ecm.uploadpdfcontent.otcsws.vo.OTCSWSConnectionResponse;
import br.tim.ecm.uploadpdfcontent.otcsws.vo.OTCSWSFileUploadResponse;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeoutException;


/**
 * OTCSWS File REST API functions
 * 
 * @author daniel.pereira
 *
 */
@Service
public class OTCSWSFileOperations {

	@Autowired
	private OTCSWSConnection connection;

	@Autowired
	private OTCSWSAuthentication authentication;

	private static Logger LOGGER = LoggerFactory.getLogger(OTCSWSFileOperations.class);

	private final static String NODES_ENDPOINT = "/api/v1/nodes";

	/**
	 * Number of retries to be executed if authentication fail
	 */
	private static final int AUTH_RETRIES = 5;

	/**
	 * Upload multiple files
	 * 
	 * @return
	 */
	public Map<String, Long> startUpload(FileUpload[] files, int parentId) {

		Map<String, Long> fileObjectID = new HashMap<String, Long>();

		if (parentId > 0) {
			for (FileUpload fileData : files) {
				try {
					long uploadedId = this.uploadFile(new FileSystemResource(fileData.getFile()),
							fileData.getContentId());
					fileObjectID.put(fileData.getFile().getName(), uploadedId);
				} catch (OTCSWSFileExceptions e) {
					LOGGER.error(e.getMessage(), e);
				}
			}
		}

		return fileObjectID;

	}

	/**
	 * Download multiple files
	 *
	 * @param documentsID
	 * @return
	 */
	public Map<Long, InputStream> startDownload(Map<Long, String> documentsID) throws IOException {

		Map<Long, InputStream> downloadedFiles = new HashMap<Long, InputStream>();

		for (Entry<Long, String> documentId : documentsID.entrySet()) {
			try {
				InputStream stream = this.downloadFileStream(documentId.getKey(), documentId.getValue());
				downloadedFiles.put(documentId.getKey(), stream);
			} catch (OTCSWSFileExceptions e) {
				LOGGER.error(e.getMessage(), e);
			}
		}

		return downloadedFiles;

	}

	/**
	 * Uploads a file to Content Server
	 *
	 * @param file
	 * @param
	 * @return
	 * @throws OTCSWSFileExceptions
	 * @throws JsonSyntaxException
	 */
	public Long uploadFile(Resource file, String idContentServer, Long parentId) throws OTCSWSFileExceptions {

		try {

			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

			String uri = NODES_ENDPOINT;
			HttpMethod method = HttpMethod.POST;

			MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<String, Object>();
			requestBody.add("type", "144");
			requestBody.add("parent_id", parentId);
			requestBody.add("name", file.getFilename());
			requestBody.add("file", file);

			int i = 0;
			OTCSWSConnectionResponse exchange;
			do {

				requestHeaders.set("OTCSTicket", authentication.getToken(idContentServer));
				exchange = this.connection.exchange(uri, method, requestHeaders, requestBody, null, OTCSWSResourceType.JSON,
						idContentServer, 0);

				if (exchange.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
					i++;
					LOGGER.info("Error while uploading the file [%s]. Retry number [%d]", file.getFilename(), i);
					this.authentication.expireToken(idContentServer);
				} else {
					break;
				}

			} while (i < AUTH_RETRIES);

			OTCSWSFileUploadResponse jsonResponse = new Gson().fromJson((String) exchange.getPayload(),
					OTCSWSFileUploadResponse.class);
			if (jsonResponse == null) {
				return 0L;
			}
			return jsonResponse.getId();

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new OTCSWSFileExceptions(String.format("Error while uploading the file [%s]", file.getFilename()));
		}

	}

	public long uploadFile(Resource file, String idContentServer) throws OTCSWSFileExceptions {
		return this.uploadFile(file, idContentServer, 2000L);
	}

	/**
	 * Uploads a file to Content Server
	 *
	 * @param token
	 * @param path
	 * @return
	 * @throws OTCSWSFileExceptions
	 */
	public long uploadFile(String path, String idContentServer) throws OTCSWSFileExceptions {

		FileSystemResource fileSystemResource = new FileSystemResource(path);
		return this.uploadFile(fileSystemResource, idContentServer);

	}

	/**
	 * Download a file
	 *
	 * @param token
	 * @param id
	 * @return
	 * @throws OTCSWSFileExceptions
	 */
	protected byte[] downloadFile(long id, String idContentServer) throws IOException {

		try {

			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

			String uri = NODES_ENDPOINT + "/" + id + "/content";
			HttpMethod method = HttpMethod.GET;

			int i = 0;
			OTCSWSConnectionResponse exchange;
			do {

				String token = this.authentication.getToken(idContentServer);
				requestHeaders.set("OTCSTicket", token);

				exchange = this.connection.exchange(uri, method, requestHeaders, null, null, OTCSWSResourceType.BYTE_ARRAY, idContentServer, 0);

				if (exchange.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
					i++;
					LOGGER.warn(String.format("Error while downloading the file [%d]. Retry number [%d]", id, i));
					this.authentication.expireToken(idContentServer);
				} else {
					break;
				}

			} while (i < AUTH_RETRIES);

			return (byte[]) exchange.getPayload();

		} catch (InterruptedException|TimeoutException e) {
			throw new OTCSWSFileExceptions(String.format("Error while downloading the file [%d] => [%s]", id, e.getMessage()));
		}

	}

	protected InputStream downloadFileAsResource(long id, String idContentServer) throws IOException {

		try {

			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

			String uri = NODES_ENDPOINT + "/" + id + "/content";
			HttpMethod method = HttpMethod.GET;

			int i = 0;
			OTCSWSConnectionResponse exchange;
			do {

				String token = this.authentication.getToken(idContentServer);
				requestHeaders.set("OTCSTicket", token);

				exchange = this.connection.exchange(uri, method, requestHeaders, null, null, OTCSWSResourceType.RESOURCE, idContentServer, 0);

				if (exchange.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
					i++;
					LOGGER.warn(String.format("Error while downloading the file [%d]. Retry number [%d]", id, i));
					this.authentication.expireToken(idContentServer);
				} else {
					break;
				}

			} while (i < AUTH_RETRIES);

			return ((InputStream) exchange.getPayload());

		} catch (InterruptedException|TimeoutException e) {
			throw new OTCSWSFileExceptions(String.format("Error while downloading the file [%d] => [%s]", id, e.getMessage()));
		}

	}

	/**
	 * Download a file
	 *
	 * @param token
	 * @param id
	 * @param filename
	 * @param file
	 * @throws OTCSWSFileExceptions
	 */
	public boolean downloadFile(long id, File file, String contentServerId) throws IOException {

		// Dowload the file
		byte[] bytes = this.downloadFile(id, contentServerId);

		// Write the bytes
		try {

			FileOutputStream fileOuputStream = new FileOutputStream(file);
			fileOuputStream.write(bytes);
			fileOuputStream.close();

			return true;

		} catch (IOException e) {

			LOGGER.error(e.getMessage(), e);
			return false;

		}

	}

	/**
	 * Download a file
	 *
	 * @return
	 * @throws OTCSWSFileExceptions
	 */
	public InputStream downloadFileStream(long id, String contentServerId) throws OTCSWSFileExceptions, IOException {

		// Dowload the file
		byte[] bytes = this.downloadFile(id, contentServerId);
		return new ByteArrayInputStream(bytes);

	}

	public InputStream downloadFileStreamHttp(long id, String contentServerId) throws IOException {
		return downloadFileAsResource(id, contentServerId);
	}


	/**
	 * Check if a file exists
	 *
	 * @param id
	 * @return
	 * @throws InterruptedException
	 */
	public boolean checkFileExists(long id, String contentServerId) throws IOException, TimeoutException, InterruptedException {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

		String uri = NODES_ENDPOINT + "/" + id;
		HttpMethod method = HttpMethod.GET;

		int i = 0;
		OTCSWSConnectionResponse exchange;
		do {

			requestHeaders.set("OTCSTicket", this.authentication.getToken(contentServerId));
			exchange = this.connection.exchange(uri, method, requestHeaders, null, null, OTCSWSResourceType.BYTE_ARRAY, contentServerId, 0);

			if (exchange.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
				i++;
				String.format("Error while checking if the file [%d] exists. Retry number [%d]", id, i);
				this.authentication.expireToken(contentServerId);
			} else {
				break;
			}

		} while (i < AUTH_RETRIES);

		return !exchange.getStatusCode().equals(HttpStatus.BAD_REQUEST);

	}

	public HttpStatus deleteFile(long id, String idContentServer) throws OTCSWSFileExceptions {
		try {
			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

			String uri = NODES_ENDPOINT + "/" + id;
			HttpMethod method = HttpMethod.DELETE;

			HttpStatus finalStatus;

			int i = 0;
			OTCSWSConnectionResponse exchange;
			do {

				requestHeaders.set("OTCSTicket", authentication.getToken(idContentServer));
				exchange = this.connection.exchange(uri, method, requestHeaders, null, null, OTCSWSResourceType.JSON, idContentServer, 0);
				finalStatus = exchange.getStatusCode();
				if (exchange.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
					i++;
					LOGGER.info("Error while deleting the file [%d]. Retry number [%d]", id, i);
					this.authentication.expireToken(idContentServer);
				} else {
					break;
				}

			} while (i < AUTH_RETRIES);
			return finalStatus;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new OTCSWSFileExceptions(String.format("Error while deleting the file [%d]", id));
		}
	}
}
