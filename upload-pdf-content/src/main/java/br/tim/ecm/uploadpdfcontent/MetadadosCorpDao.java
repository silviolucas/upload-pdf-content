package br.tim.ecm.uploadpdfcontent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;


@Service
public class MetadadosCorpDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    String querySelectDtreeCore = "SELECT DATAID FROM %s.DTREECORE " +
            " WHERE NAME = ? ";


    String querySelect = "SELECT * FROM METADADOS_CORP " +
                         " WHERE INVOICE_ID = ? AND INVOICE_ID_DT4 IN ('DT2', 'DT3') ";

    String querySelectByFileName = "SELECT * FROM METADADOS_CORP " +
            " WHERE FILE_NAME = ? AND INVOICE_ID_DT4 IN ('DT2', 'DT3') ";


    String queryUpdatePdfFull = "UPDATE METADADOS_CORP SET PDF_FULL = ? WHERE INVOICE_ID = ? AND INVOICE_ID_DT4 IN ('DT2', 'DT3')";

    String queryUpdatePdfDetalhado = "UPDATE METADADOS_CORP SET PDF_DETALHADA = ? WHERE INVOICE_ID = ? AND INVOICE_ID_DT4 IN ('DT2', 'DT3')";

    String queryUpdatePdfResumida = "UPDATE METADADOS_CORP SET PDF_RESUMIDA = ? WHERE INVOICE_ID = ? AND INVOICE_ID_DT4 IN ('DT2', 'DT3')";

//    public MetadadosCorp getInvoice(String invoiceId) {
//        return jdbcTemplate.query(querySelect, new Object[]{invoiceId}, new ResultSetExtractor<MetadadosCorp>() {
//            @Override
//            public MetadadosCorp extractData(ResultSet resultSet) throws SQLException, DataAccessException {
//                MetadadosCorp m = new MetadadosCorp();
//                m.setCsId(resultSet.getString("CS_ID"));
//                m.setCutDate(resultSet.getDate("CUT_DATE"));
//                m.setFileName(resultSet.getString("FILE_NAME"));
//                m.setInvoiceId(resultSet.getString("INVOICE_ID"));
//                m.setLxfDetalhada(resultSet.getLong("LXF_DETALHADA"));
//                m.setLxfFull(resultSet.getLong("LXF_FULL"));
//                m.setLxfResumida(resultSet.getLong("LXF_RESUMIDA"));
//                m.setParentId(resultSet.getLong("PARENT_ID"));
//                System.out.println(String.format("Retrieved Invoice Id %s => %s", invoiceId, m.toString()));
//                return m;
//            }
//        });
//    }

    public MetadadosCorp getInvoiceByFileName(String fileName) {
        return jdbcTemplate.query(querySelectByFileName, new Object[]{fileName}, new ResultSetExtractor<MetadadosCorp>() {
            @Override
            public MetadadosCorp extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                resultSet.next();
                MetadadosCorp m = new MetadadosCorp();
                m.setCsId(resultSet.getString("CS_ID"));
                m.setCutDate(resultSet.getDate("CUT_DATE"));
                m.setFileName(resultSet.getString("FILE_NAME"));
                m.setInvoiceId(resultSet.getString("INVOICE_ID"));
                m.setLxfDetalhada(resultSet.getLong("LXF_DETALHADA"));
                m.setLxfFull(resultSet.getLong("LXF_FULL"));
                m.setLxfResumida(resultSet.getLong("LXF_RESUMIDA"));
                m.setParentId(resultSet.getLong("PARENT_ID"));
                System.out.println(String.format("Retrieved Invoice by name %s => %s", fileName, m.toString()));
                return m;
            }
        });
    }


    public void updatePdfFull(String invoiceId, Long pdfFull) {
        int rowsAffected = jdbcTemplate.update(queryUpdatePdfFull, new Object[]{pdfFull, invoiceId});
        System.out.println(String.format("Updated Invoice Id %s => Pdf Full %d (%d rows)", invoiceId, pdfFull, rowsAffected));
    }

    public void updatePdfDetalhado(String invoiceId, Long pdfDetalhado) {
        int rowsAffected = jdbcTemplate.update(queryUpdatePdfDetalhado, new Object[]{pdfDetalhado, invoiceId});
        System.out.println(String.format("Updated Invoice Id %s => Pdf Detalhado %d (%d rows)", invoiceId, pdfDetalhado, rowsAffected));
    }
    public void updatePdfResumida(String invoiceId, Long pdfResumida) {
        int rowsAffected = jdbcTemplate.update(queryUpdatePdfResumida, new Object[]{pdfResumida, invoiceId});
        System.out.println(String.format("Updated Invoice Id %s => Pdf Resumida %d (%d rows)", invoiceId, pdfResumida, rowsAffected));
    }

    @Autowired
    Environment env;

    public Long checkFileContent(String fileName, String csId) {
        String schema = env.getProperty("cs.schema." + csId);
        String query = String.format(querySelectDtreeCore, schema);
        return jdbcTemplate.queryForObject(query, new Object[]{fileName}, Long.class);
    }



}
