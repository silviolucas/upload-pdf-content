package br.tim.ecm.uploadpdfcontent.otcsws.vo;

/**
 * File Upload response JSON
 * @author daniel.pereira
 *
 */
public class OTCSWSFileUploadResponse {

	private long id = 0L;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public final int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public final boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof OTCSWSFileUploadResponse))
			return false;
		OTCSWSFileUploadResponse other = (OTCSWSFileUploadResponse) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
