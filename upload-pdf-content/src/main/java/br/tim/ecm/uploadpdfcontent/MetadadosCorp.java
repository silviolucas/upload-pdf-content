package br.tim.ecm.uploadpdfcontent;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class MetadadosCorp {
	private String invoiceId;

	private Long xmlId;

	private String fileName;

	private String csId;

	private Long parentId;

	private Long pdfResumida;

	private Long pdfDetalhada;

	private Long pdfFull;

	private Long lxfResumida;

	private Long lxfDetalhada;

	private Long lxfFull;
	
	private Date cutDate;


	
}
