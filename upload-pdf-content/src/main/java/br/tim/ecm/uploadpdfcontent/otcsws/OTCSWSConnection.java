package br.tim.ecm.uploadpdfcontent.otcsws;

import br.tim.ecm.uploadpdfcontent.otcsws.vo.OTCSWSConnectionResponse;
import br.tim.ecm.uploadpdfcontent.utils.CryptorUtils;
import com.google.common.collect.Sets;
import org.apache.http.client.HttpClient;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Provides OT REST API Connections
 * @author daniel.pereira
 *
 */
@Service
public class OTCSWSConnection {

	@Autowired
	@Qualifier("customRestTemplate")
	private RestTemplate restTemplate;
	
	static String PROP_URL = "ws.url.cs.rest.context.%s";
	
	@Autowired
	private Environment env;
	
	@Value("${http.client.ssl.trust-store-enabled:false}")
	private boolean isSSL;
	
	@Value("${http.client.ssl.trust-store:null}")
	private String trustStore;
	
	@Value("${http.client.ssl.trust-store-password:null}")
    private char[] trustStorePassword;
	
	/**
	 * Number of retries to be executed if some error occur
	 */
	private static final int MAX_RETRIES = 5;
	
	/**
	 * Valid status codes for considering a valid response code from Content Server
	 */
	private static final Set<HttpStatus> VALID_RESPONSE_CODES = Sets.newHashSet(
		HttpStatus.OK,
		HttpStatus.CREATED,
		HttpStatus.ACCEPTED,
		HttpStatus.UNAUTHORIZED // This was included here, because this kind of error is handled at OTCSWSFileOperations
	);
	
	private static Logger LOGGER = LoggerFactory.getLogger(OTCSWSConnection.class);

	/**
	 * Exchange information with the server
	 * @param uri
	 * @param method
	 * @param requestHeaders
	 * @param requestBody
	 * @param uriVariables
	 * @return
	 */
	public OTCSWSConnectionResponse exchange(String uri, HttpMethod method,
                                             HttpHeaders requestHeaders,
                                             MultiValueMap<String, ?> requestBody,
                                             Map<String, String> uriVariables,
                                             OTCSWSResourceType resourceType, String contentServerId, int retries) throws IOException {

		long startTime = System.currentTimeMillis();

		OTCSWSConnectionResponse response = new OTCSWSConnectionResponse(resourceType);

		try {

			// Creates the http request entity
			HttpEntity<MultiValueMap<String, ?>> requestEntity = new HttpEntity<MultiValueMap<String, ?>>(
				requestBody == null ? new LinkedMultiValueMap<String, String>() : requestBody,
				requestHeaders
			);

			// Log the request
			LOGGER.debug( "Req #" + startTime + "> "  + requestEntity.toString());

			// Executes it
			if( resourceType.equals(OTCSWSResourceType.JSON) ) {

				ResponseEntity<String> exchange = restTemplate.exchange(
					getUrlContent(contentServerId) + uri,
					method,
					requestEntity,
					String.class,
					uriVariables == null ? new HashMap<String, String>() : uriVariables
				);

				long timeSpent = System.currentTimeMillis() - startTime;
				LOGGER.debug( "Resp took #" + timeSpent + "> "  + exchange.toString());

				HttpStatus statusCode = exchange.getStatusCode();

				// Invalid responses will be handle this way
				if( !VALID_RESPONSE_CODES.contains( statusCode ) ){
					throw new HttpClientErrorException(statusCode);
				}

				response.setPayload(exchange.getBody());
				response.setStatusCode(statusCode);

			} else  if (resourceType.equals(OTCSWSResourceType.BYTE_ARRAY))  {
				
				ResponseEntity<byte[]> exchange = restTemplate.exchange(
					getUrlContent(contentServerId) + uri,
					method,
					requestEntity,
					byte[].class,
					uriVariables == null ? new HashMap<String, String>() : uriVariables
				);

				long timeSpent = System.currentTimeMillis() - startTime;
				LOGGER.debug( "Resp took #" + timeSpent + "> "  + exchange.toString());
				
				HttpStatus statusCode = exchange.getStatusCode();
				
				// Invalid responses will be handle this way
				if( !VALID_RESPONSE_CODES.contains( statusCode ) ){
					throw new HttpClientErrorException(statusCode);
				}
				
				response.setPayload(exchange.getBody());
				response.setStatusCode(statusCode);
				
			} else if (resourceType.equals(OTCSWSResourceType.RESOURCE)) {
				// streaming the response direct to the caller
				URL url = new URL(getUrlContent(contentServerId) + uri);
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				requestHeaders.entrySet().stream().forEach(stringListEntry -> {
					connection.setRequestProperty(stringListEntry.getKey(), stringListEntry.getValue().get(0));
				});
				connection.setAllowUserInteraction(false);
				connection.setInstanceFollowRedirects(true);
				connection.setRequestMethod("GET");

				long timeSpent = System.currentTimeMillis() - startTime;
				LOGGER.debug( "Resp took #" + timeSpent + "> "  + url);

				HttpStatus statusCode = HttpStatus.valueOf(connection.getResponseCode());

				// Invalid responses will be handle this way
				if( !VALID_RESPONSE_CODES.contains( statusCode ) ){
					throw new HttpClientErrorException(statusCode);
				}
				if (statusCode.is2xxSuccessful())
					response.setPayload(connection.getInputStream());
				response.setStatusCode(statusCode);
			}
			
		} catch(HttpStatusCodeException e){

			HttpStatus statusCode = e.getStatusCode();
			String statusText = e.getStatusText();

			response.setStatusCode(statusCode);
			
			String logStr = String.format("Error: content server responded with [%s - %s]", statusCode.toString(), statusText);
			LOGGER.warn( "Resp #" + startTime + "> "  + logStr );
			
			/**
			 * Trying again
			 * This was done due to Content Server instability
			 */
			if( (statusCode.equals( HttpStatus.BAD_REQUEST ) || statusCode.equals( HttpStatus.HTTP_VERSION_NOT_SUPPORTED )) && retries < MAX_RETRIES ){
				
				LOGGER.info( String.format("Error while communicating with Content Server (Errors 400 or 505). Retry number [%d]", retries + 1) );
				return this.exchange(uri, method, requestHeaders, requestBody, uriVariables, resourceType, contentServerId, retries + 1);
				
			}
			
		} finally {
			
			long endTime = System.currentTimeMillis() - startTime;
			LOGGER.info( String.format("OTCS time [%d ms]", endTime) );
			
		}
		
		return response;
		
	}
	 
    public ClientHttpRequestFactory httpRequestFactory() {
        return new HttpComponentsClientHttpRequestFactory(httpClient());
    }

    public HttpClient httpClient() {

        // Trust own CA and all child certs
        Registry<ConnectionSocketFactory> socketFactoryRegistry = null;
        try {
        	CryptorUtils cryptor = new CryptorUtils("T1mC0nT4");
    		String trustStorePasswordString = String.valueOf(trustStorePassword);
    		trustStorePassword = cryptor.decryptOrReturnSame(trustStorePasswordString).toCharArray();
            SSLContext sslContext = SSLContexts
                    .custom()
                    .loadTrustMaterial(new File(trustStore),
                            trustStorePassword)
                    .build();

            // Since only our own certs are trusted, hostname verification is probably safe to bypass
            SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext,
                    new HostnameVerifier() {

                        @Override
                        public boolean verify(final String hostname,
                                final SSLSession session) {
                            return true;
                        }
            });

            socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.getSocketFactory())
                    .register("https", sslSocketFactory)
                    .build();           

        } catch (Exception e) {
        	LOGGER.error("Error: stablish SSL connection failed: " + e.getMessage());
            e.printStackTrace();
        }

        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        connectionManager.setMaxTotal(1);
        // This client is for internal connections so only one route is expected
        connectionManager.setDefaultMaxPerRoute(1);
        return HttpClientBuilder.create()
                .setConnectionManager(connectionManager)
                .disableCookieManagement()
                .disableAuthCaching()
                .build();
    }

    @Autowired
    public RestTemplate setRestTemplate() {
        //this.restTemplate = new RestTemplate();
        if(isSSL)
        	restTemplate.setRequestFactory(httpRequestFactory());
        return restTemplate;
    }
    
    /**
     * Gets content server URL by contentServerId
     * @param contentServerId
     * @return
     */
    private String getUrlContent(String contentServerId){
    	String property = String.format(PROP_URL, contentServerId.toLowerCase());
		String url = env.getProperty(property);
		if(url != null){
			return url;
		}
		throw new RuntimeException("property doesn't exist: " + property);
    }
	
}
