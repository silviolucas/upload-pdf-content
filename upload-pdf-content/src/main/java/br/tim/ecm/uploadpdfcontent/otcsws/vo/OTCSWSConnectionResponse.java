package br.tim.ecm.uploadpdfcontent.otcsws.vo;

import br.tim.ecm.uploadpdfcontent.otcsws.OTCSWSResourceType;
import org.springframework.http.HttpStatus;

/**
 * Represents a response from CS REST API
 * @author daniel.pereira
 *
 */
public class OTCSWSConnectionResponse {

	private Object payload = null;
	private HttpStatus statusCode;
	private final OTCSWSResourceType resourceType;

	/**
	 * Minimal constructor
	 */
	public OTCSWSConnectionResponse(OTCSWSResourceType resourceType) {
		this.resourceType = resourceType;

	}

	public Object getPayload() {
		return payload;
	}

	public void setPayload(Object payload) {
		this.payload = payload;
	}

	public HttpStatus getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}

	public OTCSWSResourceType getResourceType() {
		return resourceType;
	}

	@Override
	public final int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((payload == null) ? 0 : payload.hashCode());
		result = prime * result + ((statusCode == null) ? 0 : statusCode.hashCode());
		return result;
	}

	@Override
	public final boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof OTCSWSConnectionResponse))
			return false;
		OTCSWSConnectionResponse other = (OTCSWSConnectionResponse) obj;
		if (payload == null) {
			if (other.payload != null)
				return false;
		} else if (!payload.equals(other.payload))
			return false;
		if (statusCode != other.statusCode)
			return false;
		return true;
	}

}
