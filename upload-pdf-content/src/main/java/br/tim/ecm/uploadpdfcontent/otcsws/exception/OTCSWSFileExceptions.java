package br.tim.ecm.uploadpdfcontent.otcsws.exception;

import java.io.IOException;

/**
 * Represents file handling exceptions
 * @author daniel.pereira
 *
 */
public class OTCSWSFileExceptions extends IOException {

	private static final long serialVersionUID = 4413724475426494202L;

	public OTCSWSFileExceptions(String msg){
		super(msg);
	}
	
}
