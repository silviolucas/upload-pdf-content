package br.tim.ecm.uploadpdfcontent;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.StringUtils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.util.logging.Logger;

@Configuration
public class WebDatabaseConfiguration {


    @Autowired
    Environment env;

    @Bean(destroyMethod = "")
    DataSource dataSource() {
        BasicDataSource driverMangerDS = new BasicDataSource();
        driverMangerDS.setInitialSize(Integer.parseInt(env.getProperty("db.oracle.pool.size")));
        driverMangerDS.setDriverClassName(env.getProperty("db.oracle.driverClassName"));
        driverMangerDS.setUrl(env.getProperty("db.oracle.url"));
        driverMangerDS.setUsername(env.getProperty("db.oracle.username"));
        driverMangerDS.setPassword(env.getProperty("db.oracle.password"));
        return driverMangerDS;
    }


    @Bean
    JdbcTemplate template(DataSource dataSource) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        return template;
    }

}
