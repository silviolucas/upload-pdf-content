package br.tim.ecm.uploadpdfcontent.otcsws;

/**
 * Authentication VO
 * @author daniel.pereira
 *
 */
public class OTCSWSAuthVO {

	private String ticket;

	/**
	 * Minimal constructor
	 */
	public OTCSWSAuthVO(){}
	
	/**
	 * Full constructor
	 * @param ticket
	 */
	public OTCSWSAuthVO(String ticket) {
		this.ticket = ticket;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	
}
