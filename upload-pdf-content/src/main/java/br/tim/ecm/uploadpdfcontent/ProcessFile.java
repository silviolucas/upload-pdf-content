package br.tim.ecm.uploadpdfcontent;

import br.tim.ecm.uploadpdfcontent.otcsws.OTCSWSFileOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.PathResource;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class ProcessFile {


    public ProcessFile(OTCSWSFileOperations contentServer) {
        this.contentServer = contentServer;
    }

    public enum InvoiceType {
        RESUMIDA, DETALHADA, FULL
    }

    public static final String PDF_SUMMARY_SUFFIX = "_IR.pdf";
    public static final String PDF_DETAILED_SUFFIX = "_ID.pdf";
    public static final String PDF_FULL_SUFFIX = ".pdf";
    public static final String XML_SUFFIX = ".xml";

    @Autowired
    private MetadadosCorpDao metadadosCorpDao;

    private OTCSWSFileOperations contentServer;

    public String getXmlName(String fileName) {
        return fileName //
                .replace(PDF_SUMMARY_SUFFIX, XML_SUFFIX) // Summary
                .replace(PDF_DETAILED_SUFFIX, XML_SUFFIX) // Detailed
                .replace(PDF_FULL_SUFFIX, XML_SUFFIX); // Full
    }

    public boolean isSummary(String fileName) {
        return fileName.endsWith(PDF_SUMMARY_SUFFIX);
    }

    public boolean isDetailed(String fileName) {
        return fileName.endsWith(PDF_DETAILED_SUFFIX);
    }

    public InvoiceType getInvoiceTypeFromFileName(String fileName) {
        if (isSummary(fileName)) {
            return InvoiceType.RESUMIDA;
        }
        if (isDetailed(fileName)) {
            return InvoiceType.DETALHADA;
        }
        return InvoiceType.FULL;
    }

    public void processFile(File file) {

        System.out.println("Processando => " + file.getAbsolutePath());

        String fileName = file.getName();

        if (fileName.endsWith(".xml")) {
            System.out.println("Skipping:" + fileName);
            return;
        }

        String xmlFileName = getXmlName(fileName);

        MetadadosCorp metadadosCorp = metadadosCorpDao.getInvoiceByFileName(xmlFileName);

        InvoiceType invoiceType = getInvoiceTypeFromFileName(fileName);

        // Upload the file to ContentServer
        Long fileId;
        try {
            fileId = contentServer.uploadFile(new PathResource(file.getAbsolutePath()), metadadosCorp.getCsId(),
                    metadadosCorp.getParentId());
            if (fileId == 0L) {
                fileId = metadadosCorpDao.checkFileContent(file.getName(), metadadosCorp.getCsId());
                if (fileId == 0L) {
                    System.out.println(String.format("Arquivo %s não foi importado para o content: %s", fileName));
                    return;
                }
            }
        } catch (Exception e) {
            System.err.println(String.format("Arquivo %s não foi importado para o content: %s => %s",fileName, e.getMessage()));
            return;
        }

        System.out.println("PDF uploaded finished [fileId: " + fileId + "] => " + fileName);

        // Set the upload end timestamp
        // Set the file ID
        switch (invoiceType) {
            case RESUMIDA:
                metadadosCorpDao.updatePdfResumida(metadadosCorp.getInvoiceId(), fileId);
                break;
            case DETALHADA:
                metadadosCorpDao.updatePdfDetalhado(metadadosCorp.getInvoiceId(), fileId);
                break;
            default:
                metadadosCorpDao.updatePdfFull(metadadosCorp.getInvoiceId(), fileId);
                break;
        }

        System.out.println(String.format("File processed => %s => %s", fileName, metadadosCorp.getInvoiceId()));
    }
}
