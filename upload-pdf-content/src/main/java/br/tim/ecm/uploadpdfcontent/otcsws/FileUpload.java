package br.tim.ecm.uploadpdfcontent.otcsws;

import java.io.File;

/**
 * Wrapper with file upload information
 * @author lucas.borges
 *
 */
public class FileUpload {

	private File file;
	private String contentId;
	
	public FileUpload(File file, String contentId) {
		this.file = file;
		this.contentId = contentId;
	}
	
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}
	public String getContentId() {
		return contentId;
	}
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

}
