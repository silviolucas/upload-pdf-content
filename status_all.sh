#!/bin/bash

if [ "${FROM_JENKINS}x" = "x" ];
then
    PROJECTS_HOME="${HOME}/Projects"
else
    PROJECTS_HOME=".."
fi

if [ "${BITBUCKET_USR}x" = "x" ];
then
    echo 'Variable ${BITBUCKET_USR} has not been defined'
    exit 1
fi

USE_HTTPS="no"

if [ "${USE_HTTPS}" = "yes" ];
then
    PROTOCOL_PREFIX='https://'
else
    PROTOCOL_PREFIX='ssh://'
fi

echo "Checking ecm-corporate"
cd "${PROJECTS_HOME}/ecm-corporate" && git status

echo "Checking commissioning"
cd "${PROJECTS_HOME}/ecm-corporate/commissioning" && git status

echo "Checking commons-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/commons-corporate" && git status

echo "Checking carga-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/carga-corporate" && git status

echo "Checking notification-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/notification-corporate" && git status

echo "Checking solicitacao-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/solicitacao-corporate" && git status

echo "Checking testutils-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/testutils-corporate" && git status

echo "Checking cryptor-utils"
cd "${PROJECTS_HOME}/ecm-corporate/cryptor-utils" && git status

echo "Checking relaysbatch-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/relaysbatch-corporate" && git status

echo "Checking loadinterruptionbatch-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/loadinterruptionbatch-corporate" && git status

echo "Checking contestacao-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/contestacao-corporate" && git status

echo "Checking contaonline-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/contaonline-corporate" && git status

