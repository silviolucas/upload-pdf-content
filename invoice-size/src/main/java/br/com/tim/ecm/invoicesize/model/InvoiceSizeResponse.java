package br.com.tim.ecm.invoicesize.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class InvoiceSizeResponse {

    public static final String OK = "OK";
    public static final String ERROR = "ERROR";

    /**
     * The return code: must be OK or ERROR
     */
    String code;
    /**
     * The invoice size in bytes
     */
    Long size;

    /**
     * The invoice pages
     */
    Long pages;

    /**
     * If the invoice size is allowed to be attached to an email
     */
    Boolean allowed;

    /**
     * If the code is ERROR this must be the error message
     */
    String message;

    public InvoiceSizeResponse() {
    }

    /**
     * This constructor must be called when a success response must be sent
     * @param code - the code, must be OK
     * @param size - the invoice size in bytes
     * @param pages - the invoice pages
     * @param allowed - if the invoice can be attached to an email
     */
    public InvoiceSizeResponse(String code, Long size, Long pages, Boolean allowed) {
        this.code = code;
        this.size = size;
        this.pages = pages;
        this.allowed = allowed;
    }

    /**

     * This constructor must be called when an error is raised during processing some request.
     * The message
     * @param code - the code, must be ERROR
     * @param message - the error message
     */
    public InvoiceSizeResponse(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getAllowed() {
        return allowed;
    }

    public void setAllowed(Boolean allowed) {
        this.allowed = allowed;
    }

    public Long getPages() {
        return pages;
    }

    public void setPages(Long pages) {
        this.pages = pages;
    }

    @Override
    public String toString() {
        return "InvoiceSizeResponse{" +
                "code='" + code + '\'' +
                ", size=" + size +
                ", pages=" + pages +
                ", allowed=" + allowed +
                ", message='" + message + '\'' +
                '}';
    }
}
