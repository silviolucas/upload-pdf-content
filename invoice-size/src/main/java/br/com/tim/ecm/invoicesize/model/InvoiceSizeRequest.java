package br.com.tim.ecm.invoicesize.model;

public class InvoiceSizeRequest {
    private String csId;
    private Long documentId;
    private InvoiceTypeRequest invoiceType;
    private DocumentTypeRequest documentType;

    public String getCsId() {
        return csId;
    }

    public void setCsId(String csId) {
        this.csId = csId;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public InvoiceTypeRequest getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(InvoiceTypeRequest invoiceType) {
        this.invoiceType = invoiceType;
    }

    public DocumentTypeRequest getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentTypeRequest documentType) {
        this.documentType = documentType;
    }

    public InvoiceSizeRequest() {
    }

    public InvoiceSizeRequest(String csId, Long documentId, InvoiceTypeRequest invoiceType, DocumentTypeRequest documentType) {
        this.csId = csId;
        this.documentId = documentId;
        this.invoiceType = invoiceType;
        this.documentType = documentType;
    }

    @Override
    public String toString() {
        return "InvoiceSizeRequest{" +
                "csId='" + csId + '\'' +
                ", documentId=" + documentId +
                ", invoiceType=" + invoiceType +
                ", documentType=" + documentType +
                '}';
    }

}
