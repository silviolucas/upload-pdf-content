package br.com.tim.ecm.invoicesize.service;

import br.com.tim.ecm.invoicesize.model.InvoiceSizeRequest;
import br.com.tim.ecm.invoicesize.model.InvoiceSizeResponse;
import br.com.tim.ecm.invoicesize.model.InvoiceTypeRequest;
import org.springframework.stereotype.Service;

@Service
public interface InvoiceSizeService {

    InvoiceSizeResponse checkDocumentSizeIsAllowed(InvoiceSizeRequest request);

}
