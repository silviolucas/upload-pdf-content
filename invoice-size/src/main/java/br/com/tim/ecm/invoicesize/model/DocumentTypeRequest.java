package br.com.tim.ecm.invoicesize.model;

public enum DocumentTypeRequest {
    PDF,
    LXF
}
