package br.com.tim.ecm.invoicesize.controller;

import br.com.tim.ecm.invoicesize.model.DocumentTypeRequest;
import br.com.tim.ecm.invoicesize.model.InvoiceSizeRequest;
import br.com.tim.ecm.invoicesize.model.InvoiceSizeResponse;
import br.com.tim.ecm.invoicesize.model.InvoiceTypeRequest;
import br.com.tim.ecm.invoicesize.service.InvoiceSizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Logger;

@Controller
public class InvoiceSizeController {

    private static final Logger LOGGER = Logger.getLogger(InvoiceSizeController.class.getName());
    @Autowired
    InvoiceSizeService invoiceSizeService;

    @RequestMapping( value = "/invoiceSize", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity<InvoiceSizeResponse> invoiceSize(@RequestBody InvoiceSizeRequest request) {
        return invoiceSizeExecute(request);
    }

    @RequestMapping( value = "/invoiceSize", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public
    ResponseEntity<InvoiceSizeResponse> invoiceSize(@RequestParam String csId,
                                                   @RequestParam Long documentId,
                                                   @RequestParam InvoiceTypeRequest invoiceType,
                                                   @RequestParam DocumentTypeRequest documentType) {
        return invoiceSizeExecute(new InvoiceSizeRequest(csId, documentId, invoiceType, documentType));
    }

    private ResponseEntity<InvoiceSizeResponse> invoiceSizeExecute(InvoiceSizeRequest request) {
        long start = System.currentTimeMillis();
        LOGGER.info(String.format("Request received: [%s]", request));
        InvoiceSizeResponse response = null;
        try {
            response = invoiceSizeService.checkDocumentSizeIsAllowed(request);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch(IllegalArgumentException e) {
            response = new InvoiceSizeResponse(InvoiceSizeResponse.ERROR, e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        catch(EmptyResultDataAccessException e) {
            response = new InvoiceSizeResponse(InvoiceSizeResponse.ERROR, "Document Not Found");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        catch(Exception e) {
            response  = new InvoiceSizeResponse(InvoiceSizeResponse.ERROR, e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        finally {
            LOGGER.info(String.format("Request [%s] => Response [%s]: [%d ms]", request, response, (System.currentTimeMillis() - start)));
        }
    }

}
