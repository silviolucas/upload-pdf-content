package br.com.tim.ecm.invoicesize.service.impl;

import br.com.tim.ecm.invoicesize.model.DocumentTypeRequest;
import br.com.tim.ecm.invoicesize.model.InvoiceSizeRequest;
import br.com.tim.ecm.invoicesize.model.InvoiceSizeResponse;
import br.com.tim.ecm.invoicesize.model.InvoiceTypeRequest;
import br.com.tim.ecm.invoicesize.service.InvoiceSizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Service
public class InvoiceSizeServiceImpl implements InvoiceSizeService {

    private final static Logger LOGGER = Logger.getLogger(InvoiceSizeServiceImpl.class.getName());

    @Autowired
    JdbcTemplate template;

    @Value("${pdf.bytes.size.res:10485760}")
    Long limitBytesSummary;

    @Value("${pdf.bytes.size.det:10485760}")
    Long limitBytesDetailed;

    @Value("${pdf.bytes.size.full:10485760}")
    Long limitBytesFull;

    @Value("${lxf.adjustment.full:1}")
    Double adjustmentFull;

    @Value("${lxf.adjustment.res:1}")
    Double adjustmentSummary;

    @Value("${lxf.adjustment.det:1}")
    Double adjustmentDetailed;

    private static final String queryDocumentFull =
            "Select MAX(DVER.VERSION), DVER.DATASIZE" +
                    "  from METADADOS_CORP META, %s.DTREECORE DTREE, %s.DVERSDATA DVER" +
                    " Where META.LXF_FULL = DTREE.DATAID" +
                    "   And DTREE.DATAID = DVER.DOCID" +
                    "   And META.LXF_FULL = ? " +
                    " Group By DVER.DATASIZE";

    private static final String queryDocumentDetalhada =
            "Select MAX(DVER.VERSION), DVER.DATASIZE" +
                    "  from METADADOS_CORP META, %s.DTREECORE DTREE, %s.DVERSDATA DVER" +
                    " Where META.LXF_DETALHADA = DTREE.DATAID" +
                    "   And DTREE.DATAID = DVER.DOCID\n" +
                    "   And META.LXF_DETALHADA = ? " +
                    " Group By DVER.DATASIZE";

    private static final String queryDocumentResumida =
            "Select MAX(DVER.VERSION), DVER.DATASIZE" +
                    "  from METADADOS_CORP META, %s.DTREECORE DTREE, %s.DVERSDATA DVER" +
                    " Where META.LXF_RESUMIDA = DTREE.DATAID" +
                    "   And DTREE.DATAID = DVER.DOCID\n" +
                    "   And META.LXF_RESUMIDA = ?" +
                    " Group By DVER.DATASIZE";


    private static final String queryDocumentPdfFull =
            "Select MAX(DVER.VERSION), DVER.DATASIZE" +
                    "  from METADADOS_CORP META, %s.DTREECORE DTREE, %s.DVERSDATA DVER" +
                    " Where META.PDF_FULL = DTREE.DATAID" +
                    "   And DTREE.DATAID = DVER.DOCID" +
                    "   And META.PDF_FULL = ?"+
                    " Group By DVER.DATASIZE";

    private static final String queryDocumentPdfDetalhada =
            "Select MAX(DVER.VERSION), DVER.DATASIZE" +
                    "  from METADADOS_CORP META, %s.DTREECORE DTREE, %s.DVERSDATA DVER" +
                    " Where META.PDF_DETALHADA = DTREE.DATAID" +
                    "   And DTREE.DATAID = DVER.DOCID\n" +
                    "   And META.PDF_DETALHADA = ?"+
                    " Group By DVER.DATASIZE";

    private static final String queryDocumentPdfResumida =
            "Select MAX(DVER.VERSION), DVER.DATASIZE" +
                    "  from METADADOS_CORP META, %s.DTREECORE DTREE, %s.DVERSDATA DVER" +
                    " Where META.PDF_RESUMIDA = DTREE.DATAID" +
                    "   And DTREE.DATAID = DVER.DOCID\n" +
                    "   And META.PDF_RESUMIDA = ?"+
                    " Group By DVER.DATASIZE";

    @Autowired
    Environment env;

    Map<String, String> schemas = new HashMap<>();

    private Long findDocumentSize(Long documentId, String csId, InvoiceTypeRequest invoiceType, DocumentTypeRequest documentType) {
        long start = System.currentTimeMillis();
        try {
            LOGGER.info(String.format("Checking CS Schema [%s] for [%d] type [%s]", csId, documentId, invoiceType.name()));
            String schema = schemas.get(csId);
            if (StringUtils.isEmpty(schema)) {
                schema = env.getProperty("cs.schema." + csId);
                schemas.put(csId, schema);
            }
            if (schema == null)
                throw new IllegalArgumentException(String.format("Invalid CSID (%s) for Document (%d) Type (%s): ", csId, documentId, invoiceType.name()));

            String query = null;
            switch (invoiceType) {
                case DETALHADA:
                    query = documentType.equals(DocumentTypeRequest.LXF) ? queryDocumentDetalhada : queryDocumentPdfDetalhada;
                    break;
                case FULL:
                    query = documentType.equals(DocumentTypeRequest.LXF) ? queryDocumentFull : queryDocumentPdfFull;
                    break;
                case RESUMIDA:
                    query = documentType.equals(DocumentTypeRequest.LXF) ? queryDocumentResumida : queryDocumentPdfResumida;
                    break;
            }

            List<Map<String,Object>> result = template.queryForList(
                    String.format(query, schema, schema), new Object[]{documentId});
            if (result.size() == 0)
                throw new EmptyResultDataAccessException(1);

            // in case of many records we use the first record because we want the last version
            return (Long)result.get(0).get("DATASIZE");
        }
        finally {
            LOGGER.info(String.format("Time elapsed checking CS Schema [%s] for [%d] type [%s]: [%d ms]", csId, documentId, invoiceType.name(), (System.currentTimeMillis() - start)));
        }

    }

    @Override
    public InvoiceSizeResponse checkDocumentSizeIsAllowed(InvoiceSizeRequest request) {
        // first check the number of pages
        Long size = findDocumentSize(request.getDocumentId(), request.getCsId(), request.getInvoiceType(), request.getDocumentType());
        // LXF demands some adjustment in the value
        if (request.getDocumentType() == DocumentTypeRequest.LXF) {
            switch (request.getInvoiceType()) {
                case FULL:
                    size = new Double(size * adjustmentFull).longValue();
                    break;
                case DETALHADA:
                    size = new Double(size * adjustmentDetailed).longValue();
                    break;
                case RESUMIDA:
                    size = new Double(size * adjustmentSummary).longValue();
                    break;
            }
        }
        // if the number of pages is ok, check the size of the document
        Boolean sizeOk = false;
        switch (request.getInvoiceType()) {
            case FULL:
                sizeOk = limitBytesFull == 0 || size <= limitBytesFull;
                break;
            case DETALHADA:
                sizeOk = limitBytesDetailed == 0 || size <= limitBytesDetailed;
                break;
            case RESUMIDA:
                sizeOk = limitBytesSummary == 0 || size <= limitBytesSummary;
                break;
        }
        return new InvoiceSizeResponse(InvoiceSizeResponse.OK, size, null, sizeOk);
    }

}
