package br.com.tim.ecm.invoicesize.model;

public enum InvoiceTypeRequest {
    RESUMIDA,
    DETALHADA,
    FULL
}
