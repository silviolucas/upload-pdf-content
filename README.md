# ecm-corporate #

This is the ECM Corporate root project. You will want to clone this repo and build it before working on specific modules.

### What is this repository for? ###

* This is a convenient project used for consolidating the depedencies management and Maven plugins management and common options.
* The current version is ecm-corporate-0.0.1-SNAPSHOT

### How do I get set up? ###

Use your git client to clone this repo in the usual way

```
git clone ssh://<<your_bitbucket_userid>>@bitbucket.org/ecm-f3-e2/ecm-corporate.git

```

Once you have cloned this root project, you will want to clone its children. For your convenience there is a script named ```clone_all.sh``` that will do that for you.

In order to execute it, you will need to set an environment variable with your BitBucket user Id. You may want to put this on your ```${HOME}/.profile``` file.

```
export BITBUCKET_USR="<<your_bitbucket_userid>>"

```

Then just execute the script and it will clone all child repositories.

```
$ ./clone_all.sh

```
