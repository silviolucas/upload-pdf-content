#!/bin/bash

# solving jenkins buid/deploy
#if [ "${HOME}" = '/var/lib/jenkins' -o "${HOME}" = '/var/lib/jenkins3' ]; then
#    mkdir "$WORKSPACE/Projects"
#    $HOME=$WORKSPACE
#fi

#rm -Rf "${PROJECTS_HOME}/ecm-corporate"


echo ">>>>>> IN CLONE_ALL.SH <<<<<<<"
echo "$(pwd)"
echo `pwd`

if [ "${FROM_JENKINS}x" = "x" ];
then
    PROJECTS_HOME="${HOME}/Projects"
else
    PROJECTS_HOME="$(pwd)/.."
fi

if [ "${BITBUCKET_USR}x" = "x" ];
then
    echo 'Variable ${BITBUCKET_USR} has not been defined'
    exit 1
fi

BRANCH_IN_USE="master"

# USE_HTTPS="no"

if [ "${USE_HTTPS}" = "yes" ];
then
    PROTOCOL_PREFIX='https://'
else
    PROTOCOL_PREFIX='ssh://'
fi

cd "${PROJECTS_HOME}"

# git clone "${PROTOCOL_PREFIX}${BITBUCKET_USR}@bitbucket.org/ecm-f3-e2/ecm-corporate.git"

cd "${PROJECTS_HOME}/ecm-corporate"

git clone "${PROTOCOL_PREFIX}${BITBUCKET_USR}@bitbucket.org/ecm-f3-e2/commissioning.git"
git clone "${PROTOCOL_PREFIX}${BITBUCKET_USR}@bitbucket.org/ecm-f3-e2/commons-corporate.git"
git clone "${PROTOCOL_PREFIX}${BITBUCKET_USR}@bitbucket.org/ecm-f3-e2/carga-corporate.git"
git clone "${PROTOCOL_PREFIX}${BITBUCKET_USR}@bitbucket.org/ecm-f3-e2/notification-corporate.git"
git clone "${PROTOCOL_PREFIX}${BITBUCKET_USR}@bitbucket.org/ecm-f3-e2/solicitacao-corporate.git"
git clone "${PROTOCOL_PREFIX}${BITBUCKET_USR}@bitbucket.org/ecm-f3-e2/testutils-corporate.git"
git clone "${PROTOCOL_PREFIX}${BITBUCKET_USR}@bitbucket.org/ecm-f3-e2/cryptor-utils.git"
git clone "${PROTOCOL_PREFIX}${BITBUCKET_USR}@bitbucket.org/ecm-f3-e2/relaysbatch-corporate.git"
git clone "${PROTOCOL_PREFIX}${BITBUCKET_USR}@bitbucket.org/ecm-f3-e2/loadinterruptionbatch-corporate.git"
git clone "${PROTOCOL_PREFIX}${BITBUCKET_USR}@bitbucket.org/ecm-f3-e2/contestacao-corporate.git"
git clone "${PROTOCOL_PREFIX}${BITBUCKET_USR}@bitbucket.org/ecm-f3-e2/contaonline-corporate.git"

if [ "${BRANCH_IN_USE}x" != "x" ];
then

    echo "Pointing ecm-corporate to branch '${BRANCH_IN_USE}'"

    cd "${PROJECTS_HOME}/ecm-corporate"
    git fetch && git checkout "${BRANCH_IN_USE}"

    echo "Pointing commissioning to branch '${BRANCH_IN_USE}'"

    cd "${PROJECTS_HOME}/ecm-corporate/commissioning"
    git fetch && git checkout "${BRANCH_IN_USE}"

    echo "Pointing commons-corporate to branch '${BRANCH_IN_USE}'"

    cd "${PROJECTS_HOME}/ecm-corporate/commons-corporate"
    git fetch && git checkout "${BRANCH_IN_USE}"

    echo "Pointing carga-corporate to branch '${BRANCH_IN_USE}'"

    cd "${PROJECTS_HOME}/ecm-corporate/carga-corporate"
    git fetch && git checkout "${BRANCH_IN_USE}"

    echo "Pointing notification-corporate to branch '${BRANCH_IN_USE}'"

    cd "${PROJECTS_HOME}/ecm-corporate/notification-corporate"
    git fetch && git checkout "${BRANCH_IN_USE}"

    echo "Pointing solicitacao-corporate to branch '${BRANCH_IN_USE}'"

    cd "${PROJECTS_HOME}/ecm-corporate/solicitacao-corporate"
    git fetch && git checkout "${BRANCH_IN_USE}"

    echo "Pointing testutils-corporate to branch '${BRANCH_IN_USE}'"

    cd "${PROJECTS_HOME}/ecm-corporate/testutils-corporate"
    git fetch && git checkout "${BRANCH_IN_USE}"

    echo "Pointing cryptor-utils to branch '${BRANCH_IN_USE}'"

    cd "${PROJECTS_HOME}/ecm-corporate/cryptor-utils"
    git fetch && git checkout "${BRANCH_IN_USE}"

    echo "Pointing relaysbatch-corporate to branch '${BRANCH_IN_USE}'"

    cd "${PROJECTS_HOME}/ecm-corporate/relaysbatch-corporate"
    git fetch && git checkout "${BRANCH_IN_USE}"

    echo "Pointing loadinterruptionbatch-corporate to branch '${BRANCH_IN_USE}'"

    cd "${PROJECTS_HOME}/ecm-corporate/loadinterruptionbatch-corporate"
    git fetch && git checkout "${BRANCH_IN_USE}"

    echo "Pointing contestacao-corporate to branch '${BRANCH_IN_USE}'"

    cd "${PROJECTS_HOME}/ecm-corporate/contestacao-corporate"
    git fetch && git checkout "${BRANCH_IN_USE}"

    echo "Pointing contaonline-corporate to branch '${BRANCH_IN_USE}'"

    cd "${PROJECTS_HOME}/ecm-corporate/contaonline-corporate"
    git fetch && git checkout "${BRANCH_IN_USE}"

    cd "${PROJECTS_HOME}/ecm-corporate"

fi

echo "All done!"
