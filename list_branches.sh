#!/bin/bash

if [ "${FROM_JENKINS}x" = "x" ];
then
    PROJECTS_HOME="${HOME}/Projects"
else
    PROJECTS_HOME=".."
fi

if [ "${BITBUCKET_USR}x" = "x" ];
then
    echo 'Variable ${BITBUCKET_USR} has not been defined'
    exit 1
fi

USE_HTTPS="no"

if [ "${USE_HTTPS}" = "yes" ];
then
    PROTOCOL_PREFIX='https://'
else
    PROTOCOL_PREFIX='ssh://'
fi

echo "Pulling ecm-corporate"
cd "${PROJECTS_HOME}/ecm-corporate" && git branch

echo "Pulling commissioning"
cd "${PROJECTS_HOME}/ecm-corporate/commissioning" && git branch

echo "Pulling commons-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/commons-corporate" && git branch

echo "Pulling carga-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/carga-corporate" && git branch

echo "Pulling notification-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/notification-corporate" && git branch

echo "Pulling solicitacao-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/solicitacao-corporate" && git branch

echo "Pulling testutils-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/testutils-corporate" && git branch

echo "Pulling cryptor-utils"
cd "${PROJECTS_HOME}/ecm-corporate/cryptor-utils" && git branch

echo "Pulling relaysbatch-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/relaysbatch-corporate" && git branch

echo "Pulling loadinterruptionbatch-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/loadinterruptionbatch-corporate" && git branch

echo "Pulling contestacao-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/contestacao-corporate" && git branch

echo "Pulling contaonline-corporate"
cd "${PROJECTS_HOME}/ecm-corporate/contaonline-corporate" && git branch
