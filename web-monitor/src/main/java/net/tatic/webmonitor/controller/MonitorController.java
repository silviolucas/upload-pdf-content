package net.tatic.webmonitor.controller;

import net.tatic.webmonitor.processor.WebMonitorItem;
import net.tatic.webmonitor.processor.WebMonitorItemList;
import net.tatic.webmonitor.processor.WebMonitorResponse;
import net.tatic.webmonitor.processor.WebMonitorResponseList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@RestController
public class MonitorController {

    @Autowired
    WebMonitorItemList list;

    @Autowired
    WebMonitorResponseList responses;

    @RequestMapping(value = "/params", method = RequestMethod.GET)
    public List<WebMonitorItem> params() {
        return list.getItem();
    }

    @RequestMapping(value = "/results", method = RequestMethod.GET)
    public Map<WebMonitorItem, Collection<WebMonitorResponse>> results() {
        return responses.getResponses();
    }

}
