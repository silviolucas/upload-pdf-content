package net.tatic.webmonitor.processor;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class WebMonitorResponse {

    private WebMonitorItem item;

    private Long timeSpent;

    private HttpStatus httpStatus;

    private Exception exception;

    private Date time;
}
