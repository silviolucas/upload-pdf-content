package net.tatic.webmonitor.processor;

import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

@Getter
@Component
public class WebMonitorResponseList implements Comparator<WebMonitorResponse> {

    private Map<WebMonitorItem, Collection<WebMonitorResponse>> responses = new HashMap<>();

    public void add(WebMonitorItem item, WebMonitorResponse response) {
        if (responses.containsKey(item)) {
            responses.get(item).add(response);
        }
        else {
            FixedSizeOrderedSet<WebMonitorResponse> orderedSet = new FixedSizeOrderedSet<>(this, 16);
            orderedSet.add(response);
            responses.put(item, orderedSet);
        }
    }

    @Override
    public int compare(WebMonitorResponse o1, WebMonitorResponse o2) {
        return o1.getTime().compareTo(o2.getTime());
    }
}
