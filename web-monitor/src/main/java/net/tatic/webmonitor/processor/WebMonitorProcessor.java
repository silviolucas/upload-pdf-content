package net.tatic.webmonitor.processor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.*;
import java.util.Date;
import java.util.logging.Logger;

@Service
public class WebMonitorProcessor {


    private static final Logger logger = Logger.getLogger(WebMonitorProcessor.class.getName());

    @Autowired
    WebMonitorItemList list;

    @Autowired
    WebMonitorResponseList responseBuffer;

    private String readStringFromFile(String fileName)
            throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (InputStream is = new FileInputStream(fileName);
             BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }

    public HttpStatus process(WebMonitorItem item) throws IOException {

        WebClient.Builder clientBuilder = WebClient.builder();

        clientBuilder.baseUrl(item.getUrl());
        if (item.getHeaders() != null) {
            clientBuilder.defaultHeaders(httpHeaders -> item.getHeaders().forEach((headerName, headerValue) -> httpHeaders.add(headerName, headerValue)));
        }

        WebClient client = clientBuilder.build();

        boolean isPost = "POST".equalsIgnoreCase(item.getMethod());

        if (isPost) {
            String request = readStringFromFile(item.getRequest());
            return client.post().syncBody(request).exchange().block().statusCode();
        }

        return client.get().exchange().block().statusCode();

    }

    @Scheduled(fixedRate = 60000, initialDelay = 1000)
    public void execute() {

        list.getItem().stream().forEach(webMonitorItem -> {
            logger.info("Processando {START} => " + webMonitorItem);

            WebMonitorResponse response = new WebMonitorResponse();
            response.setItem(webMonitorItem);
            response.setTime(new Date());
            long startTime = System.currentTimeMillis();
            try {
                HttpStatus status = process(webMonitorItem);
                response.setHttpStatus(status);
            }
            catch(Exception e) {
                response.setException(e);
            }
            response.setTimeSpent(System.currentTimeMillis() - startTime);
            responseBuffer.add(webMonitorItem, response);
            logger.info("Processado {END} => " + response);

        });

    }



}
