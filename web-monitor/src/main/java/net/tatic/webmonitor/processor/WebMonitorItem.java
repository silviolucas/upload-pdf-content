package net.tatic.webmonitor.processor;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@ConfigurationProperties("monitor.item")
@EqualsAndHashCode
@ToString
public class WebMonitorItem {

    private String url;

    private String method;

    private String request;

    private Map<String, String> headers;

}
